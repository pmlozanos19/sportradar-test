package com.sportradar.sportradartest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportradarTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportradarTestApplication.class, args);
    }
}
