package com.sportradar.sportradartest.repository;

import com.sportradar.sportradartest.domain.Match;
import com.sportradar.sportradartest.domain.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Repository to match
 */
public interface MatchRepository extends PagingAndSortingRepository<Match, Long> {
    /**
     * @return all match
     */
    List<Match> findAll();

    /**
     * Find by home team optional.
     *
     * @param homeTeam the home team
     * @return the optional
     */
    Optional<Match> findByHomeTeam(String homeTeam);

    /**
     * Find by match active optional.
     *
     * @param homeTeam the home team
     * @param awayTeam the away team
     * @param status   the status
     * @return the optional
     */
    @Query("SELECT m From Match m WHERE ((m.homeTeam=:homeTeam AND m.awayTeam=:awayTeam) OR (m.awayTeam=:homeTeam AND m.homeTeam=:awayTeam)) AND m.status=:status")
    Optional<Match> findByMatchActive(@Param("homeTeam") String homeTeam, @Param("awayTeam") String awayTeam, @Param("status") Status status);

    /**
     * Find summary games page.
     *
     * @param pageable the pageable
     * @param status   the status
     * @return the page
     */
    @Query("SELECT m, m.homeScore - m.awayScore as totalScore From Match m WHERE (:status is null or m.status=:status) ORDER BY totalScore, m.startDate DESC")
    Page<Match> findSummaryGames(Pageable pageable, @Param("status") Status status);
}
