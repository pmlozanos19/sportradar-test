package com.sportradar.sportradartest.service.exception;


/**
 * The type Match exception.
 */
public class MatchException extends RuntimeException {
    /**
     * Instantiates a new Match exception.
     *
     * @param s the s
     */
    public MatchException(String s) {
        // Call constructor of parent Exception
        super(s);
    }
}

