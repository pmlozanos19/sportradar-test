package com.sportradar.sportradartest.service;

import com.sportradar.sportradartest.domain.Match;
import com.sportradar.sportradartest.domain.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The interface Match service.
 */
public interface MatchService {

    /**
     * Start game match.
     *
     * @param homeTeam the home team
     * @param awayTeam the away team
     * @return match match
     */
    Match startGame(String homeTeam, String awayTeam);

    /**
     * Finish game match.
     *
     * @param homeTeam the home team
     * @param awayTeam the away team
     * @return the match
     */
    Match finishGame(String homeTeam, String awayTeam);

    /**
     * Update score match.
     *
     * @param homeTeam  the home team
     * @param awayTeam  the away team
     * @param homeScore the home score
     * @param awayScore the away score
     * @return the match
     */
    Match updateScore(String homeTeam, String awayTeam, Integer homeScore, Integer awayScore);

    /**
     * Summary match total score page.
     *
     * @param pageable the pageable
     * @param status   the status
     * @return the page
     */
    Page<Match> summaryMatchTotalScore(Pageable pageable, Status status);
}
