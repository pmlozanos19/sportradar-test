package com.sportradar.sportradartest.service;

import com.sportradar.sportradartest.domain.Match;
import com.sportradar.sportradartest.domain.Status;
import com.sportradar.sportradartest.repository.MatchRepository;
import com.sportradar.sportradartest.service.exception.MatchException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Service match
 */
@Service
@Slf4j
public class MatchServiceImpl implements MatchService {

    private final MatchRepository matchRepository;


    /**
     * Instantiates a new Match service.
     *
     * @param matchRepository the match repository
     */
    @Autowired
    public MatchServiceImpl(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @Override
    public Match startGame(String homeTeam, String awayTeam) {
        Optional<Match> matchActive = matchActive(homeTeam, awayTeam);
        if (!matchActive.isEmpty()) {
            log.error("The match: " + homeTeam + " - " + awayTeam + " is playing now");
            throw new MatchException("The match: " + homeTeam + " - " + awayTeam + " is playing now");
        }
        Match match = Match
                .builder()
                .homeTeam(homeTeam)
                .awayTeam(awayTeam)
                .homeScore(0)
                .awayScore(0)
                .startDate(LocalDateTime.now())
                .endDate(null)
                .status(Status.IN_PROGRESS)
                .build();
        Match matchSave = matchRepository.save(match);
        log.info("The match started is " + matchSave);
        return matchSave;
    }

    @Override
    public Match finishGame(String homeTeam, String awayTeam) {
        validationTeamsAreCorrect(homeTeam, awayTeam);
        Optional<Match> matchActive = matchActive(homeTeam, awayTeam);
        if (matchActive.isEmpty()) {
            log.error("The match: " + homeTeam + " - " + awayTeam + " is finished or not exist");
            throw new MatchException("The match: " + homeTeam + " - " + awayTeam + " is finished or not exist");
        }
        Match matchUpdated = matchActive
                .get()
                .builder()
                .endDate(LocalDateTime.now())
                .status(Status.FINISHED)
                .build();
        matchRepository.save(matchUpdated);
        log.info("The match finished is " + matchUpdated);
        return matchUpdated;
    }

    @Override
    public Match updateScore(String homeTeam, String awayTeam, Integer homeScore, Integer awayScore) {
        validationTeamsAreCorrect(homeTeam, awayTeam);
        Optional<Match> matchActive = matchActive(homeTeam, awayTeam);
        if (matchActive.isEmpty()) {
            log.error("The match: " + homeTeam + " - " + awayTeam + " is finished or not exist");
            throw new MatchException("The match: " + homeTeam + " - " + awayTeam + " is finished or not exist");
        }
        validationScores(homeScore, awayScore, matchActive.get().getHomeScore(), matchActive.get().getAwayScore());
        //Is need valid if score is possible
        Match matchUpdated = matchActive
                .get()
                .builder()
                .homeScore(homeScore)
                .awayScore(awayScore)
                .build();
        matchRepository.save(matchUpdated);
        log.info("The match update is " + matchUpdated);
        return matchUpdated;
    }

    @Override
    public Page<Match> summaryMatchTotalScore(Pageable pageable, Status status) {
        Page<Match> summaryGames = matchRepository.findSummaryGames(pageable, status);
        summaryGames
                .getContent()
                .forEach(match -> log.info(match + "\n"));
        return summaryGames;
    }

    private void validationScores(Integer newHomeScore, Integer newAwayScore, Integer currentHomeScore, Integer currentAwayScore) {
        if (currentAwayScore > newAwayScore || currentHomeScore > newHomeScore) {
            log.error("The new scores are not less that currently");
            throw new MatchException("The new scores are not less that currently");
        } else if (!(newHomeScore.equals(currentHomeScore) && newAwayScore > currentAwayScore) && !(newAwayScore.equals(currentAwayScore) && newHomeScore > currentHomeScore)) {
            log.error("Only can sum one score");
            throw new MatchException("Only can sum one score");
        } else if (newHomeScore > currentHomeScore + 1 || newAwayScore > currentAwayScore + 1) {
            log.error("Only can sum one score and one goal");
            throw new MatchException("Only can sum one score and one goal");
        }
    }

    private Pair<String, String> validationTeamsAreCorrect(String homeTeam, String awayTeam) {
        //In the future versions it posible added list country to check correctly values
        Pair<String, String> pairTeams;
        if (StringUtils.isNoneEmpty(homeTeam) || StringUtils.isNoneEmpty(awayTeam)) {
            String homeTeamToUpperCase = homeTeam.toUpperCase();
            String awayTeamToUpperCase = awayTeam.toUpperCase();
            pairTeams = Pair.of(homeTeamToUpperCase, awayTeamToUpperCase);
            if (homeTeamToUpperCase.equals(awayTeamToUpperCase)) {
                log.error("Two teams are equals: " + homeTeamToUpperCase);
                throw new MatchException("Two teams are equals: " + homeTeamToUpperCase);
            }
        } else {
            log.error("Must add two teams valid");
            throw new MatchException("Must add two teams valid");
        }
        return pairTeams;
    }

    private Optional<Match> matchActive(String homeTeam, String awayTeam) {
        Pair<String, String> pairTeams = validationTeamsAreCorrect(homeTeam, awayTeam);
        return matchRepository.findByMatchActive(pairTeams.getLeft(), pairTeams.getRight(), Status.IN_PROGRESS);
    }

}
