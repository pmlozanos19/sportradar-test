package com.sportradar.sportradartest.domain;

/**
 * Enum to define Status of match
 */
public enum Status {
    IN_PROGRESS,
    FINISHED;
}
