package com.sportradar.sportradartest.repository;

import com.sportradar.sportradartest.domain.Match;
import com.sportradar.sportradartest.domain.Status;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Slf4j
public class MatchRepositoryTest {

    @Autowired
    MatchRepository repository;

    private Match matchOne;
    private Match matchTwo;
    private Match matchThree;

    @BeforeEach
    public void init() {
        matchOne = createMatch(1L, "SPAIN", "PORTUGAL", 2, 1, LocalDateTime.now(), Status.IN_PROGRESS);
        matchTwo = createMatch(2L, "GREECE", "ISLAND", 1, 4, LocalDateTime.now().minusHours(2), Status.IN_PROGRESS);
        matchThree = createMatch(3L, "MEXICO", "CANADA", 0, 5, LocalDateTime.now().minusHours(4), Status.FINISHED);
    }


    @Test
    @DisplayName("should find no match if repository is empty")
    public void should_find_no_match_if_repository_is_empty() {
        List<Match> matches = repository.findAll();

        //asserts
        assertNotNull(matches);
        assertTrue(matches.isEmpty());
    }

    @Test
    @DisplayName("should save match")
    public void should_save_match() {
        repository.save(matchOne);

        // asserts
        assertNotNull(matchOne);
    }

    @Test
    @DisplayName("should find 3 match")
    public void should_find_three_match() {
        repository.save(matchOne);
        repository.save(matchTwo);
        repository.save(matchThree);
        List<Match> matches = repository.findAll();

        //asserts
        assertNotNull(matches);
        assertEquals(matches.size(), 3);

    }

    @Test
    @DisplayName("save match and find by Id")
    public void save_match_and_find_by_id() {
        repository.save(matchOne);
        Match expected = repository.findByHomeTeam("SPAIN")
                .orElse(null);

        //asserts
        assertNotNull(repository.findAll());
        assertEquals(matchOne.getHomeTeam(), expected.getHomeTeam());
        assertEquals(matchOne.getAwayTeam(), expected.getAwayTeam());
    }

    @Test
    @DisplayName("should be find by match active")
    public void should_be_find_by_match_active() {
        repository.save(matchOne);
        Match expected = repository.findByMatchActive("PORTUGAL", "SPAIN", Status.IN_PROGRESS)
                .orElse(null);

        //asserts
        assertNotNull(repository.findAll());
        assertNotNull(expected);

    }

    @Test
    @DisplayName("should not be find by match active")
    public void should_not_be_find_by_match_active() {
        repository.save(matchOne);
        Match expected = repository.findByMatchActive("SPAIN", "FRANCE", Status.FINISHED)
                .orElse(null);

        //asserts
        assertNotNull(repository.findAll());
        assertNull(expected);

    }


    @Test
    @DisplayName("should show all scores sort correctly")
    public void should_show_all_scores_sort_correctly() {
        repository.save(matchOne);
        repository.save(matchTwo);
        repository.save(matchThree);
        Pageable pageable = PageRequest.of(0, 3);
        Page<Match> summaryGames = repository.findSummaryGames(pageable, null);

        //assert
        assertNotNull(summaryGames);
        assertEquals(summaryGames.getTotalElements(), 3);

    }

    @Test
    @DisplayName("should show all scores sort correctly with status in progress")
    public void should_show_all_scores_sort_correctly_with_status_in_progress() {
        repository.save(matchOne);
        repository.save(matchTwo);
        repository.save(matchThree);
        Pageable pageable = PageRequest.of(0, 3);
        Page<Match> summaryGames = repository.findSummaryGames(pageable, Status.IN_PROGRESS);

        //assert
        assertNotNull(summaryGames);
        assertEquals(summaryGames.getTotalElements(), 2);
    }

    @Test
    @DisplayName("should show all scores sort correctly with status finished")
    public void should_show_all_scores_sort_correctly_with_status_finished() {
        repository.save(matchOne);
        repository.save(matchTwo);
        repository.save(matchThree);
        Pageable pageable = PageRequest.of(0, 3);
        Page<Match> summaryGames = repository.findSummaryGames(pageable, Status.FINISHED);

        //assert
        assertNotNull(summaryGames);
        assertEquals(summaryGames.getTotalElements(), 1);
    }

    @Test
    @DisplayName("should show all scores sort correctly_empty")
    public void should_show_all_scores_sort_correctly_empty() {
        Pageable pageable = PageRequest.of(1, 2);
        Page<Match> summaryGames = repository.findSummaryGames(pageable, null);

        //assert
        assertNotNull(summaryGames);
        assertEquals(summaryGames.getTotalElements(), 0);
    }

    private final Match createMatch(Long id, String homeTeam, String awayTeam, Integer homeScore, Integer awayScore, LocalDateTime startDate, Status status) {
        LocalDateTime endDate = null;
        if (status.equals(Status.FINISHED)) {
            endDate = calculateEndMatch(startDate);
        }
        return Match
                .builder()
                .id(id)
                .homeTeam(homeTeam)
                .awayTeam(awayTeam)
                .homeScore(homeScore)
                .awayScore(awayScore)
                .startDate(startDate)
                .endDate(endDate)
                .status(status)
                .build();
    }

    private final LocalDateTime calculateEndMatch(LocalDateTime startDate) {
        //Calculate ideal time match
        return startDate.plusMinutes(105);
    }
}
