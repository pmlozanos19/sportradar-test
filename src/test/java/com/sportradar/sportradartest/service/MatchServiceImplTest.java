package com.sportradar.sportradartest.service;

import com.sportradar.sportradartest.domain.Match;
import com.sportradar.sportradartest.domain.Status;
import com.sportradar.sportradartest.repository.MatchRepository;
import com.sportradar.sportradartest.service.exception.MatchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * The type Match service impl test.
 */
@SpringBootTest
class MatchServiceImplTest {

    @MockBean
    MatchRepository repository;

    @Autowired
    MatchService matchService = new MatchServiceImpl(repository);

    private Match matchOne;
    private Match matchTwo;
    private Match matchThree;


    @BeforeEach
    public void init() {
        matchOne = createMatch(1L, "SPAIN", "PORTUGAL", 2, 1, LocalDateTime.now(), Status.IN_PROGRESS);
        matchTwo = createMatch(2L, "GREECE", "ISLAND", 1, 4, LocalDateTime.now().minusHours(2), Status.IN_PROGRESS);
        matchThree = createMatch(3L, "MEXICO", "CANADA", 0, 5, LocalDateTime.now().minusHours(4), Status.FINISHED);
    }

    @Test
    @DisplayName("should start game")
    public void should_start_game() {
        Match matchInit = createMatchInit("SPAIN", "PORTUGAL");
        when(repository.save(any(Match.class))).thenReturn(matchInit);

        Match matchExpected = matchService.startGame("SPAIN", "PORTUGAL");

        //asserts
        assertNotNull(matchExpected);
    }

    @Test
    @DisplayName("should not start game with same teams")
    public void should_not_start_game_with_same_teams() {
        Match match = createMatchInit("SPAIN", "SPAIN");
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.startGame("SPAIN", "SPAIN");
        });

        assertEquals("Two teams are equals: SPAIN", thrown.getMessage());
    }

    @Test
    @DisplayName("should not start game with same match init")
    public void should_not_start_game_with_same_match_init() {
        Match matchReplicated = createMatchInit("SPAIN", "PORTUGAL");
        Match match = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.startGame("SPAIN", "PORTUGAL");
        });

        assertEquals("The match: SPAIN - PORTUGAL is playing now", thrown.getMessage());
    }

    @Test
    @DisplayName("should finish game")
    public void should_finish_game() {
        Match matchInit = createMatchInit("SPAIN", "PORTUGAL");
        when(repository.save(any(Match.class))).thenReturn(matchInit);
        Match matchReplicated = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));

        Match matchExpected = matchService.finishGame("SPAIN", "PORTUGAL");

        //asserts
        assertNotNull(matchExpected);
    }

    @Test
    @DisplayName("should not finish game with teams incorrect")
    public void should_not_finish_game_with_teams_incorrect() {
        Match match = createMatchInit("SPAIN", "SPAIN");
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.finishGame("SPAIN", "SPAIN");
        });

        assertEquals("Two teams are equals: SPAIN", thrown.getMessage());
    }

    @Test
    @DisplayName("should not finish game when match is finished")
    public void should_not_finish_game_when_match_is_finished() {
        Match match = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.empty());
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.finishGame("SPAIN", "PORTUGAL");
        });

        assertEquals("The match: SPAIN - PORTUGAL is finished or not exist", thrown.getMessage());
    }

    @Test
    @DisplayName("should update score")
    public void should_update_score() {
        Match matchInit = createMatchInit("SPAIN", "PORTUGAL");
        when(repository.save(any(Match.class))).thenReturn(matchInit);
        Match matchReplicated = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));

        Match matchExpected = matchService.updateScore("SPAIN", "PORTUGAL", 1, 0);

        //asserts
        assertNotNull(matchExpected);
    }

    @Test
    @DisplayName("should not update score with same teams")
    public void should_not_update_score_with_same_teams() {
        Match match = createMatchInit("SPAIN", "SPAIN");
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.updateScore("SPAIN", "SPAIN", 0, 1);
        });

        assertEquals("Two teams are equals: SPAIN", thrown.getMessage());
    }

    @Test
    @DisplayName("should update score with same match init")
    public void should_update_score_with_same_match_init() {
        Match matchReplicated = createMatchInit("SPAIN", "PORTUGAL");
        Match match = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));
        when(repository.save(any(Match.class))).thenReturn(match);

        Match matchExpected = matchService.updateScore("SPAIN", "PORTUGAL", 1, 0);

        //asserts
        assertNotNull(matchExpected);
    }

    @Test
    @DisplayName("should not update score less that currently")
    public void should_not_update_score_less_that_currently() {
        Match matchReplicated = createMatchInit("SPAIN", "PORTUGAL");
        matchReplicated.setAwayScore(10);
        Match match = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("PORTUGAL", "SPAIN", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.updateScore("PORTUGAL", "SPAIN", 0, 1);
        });

        assertEquals("The new scores are not less that currently", thrown.getMessage());
    }

    @Test
    @DisplayName("should not update score only plus one score")
    public void should_not_update_score_only_plus_one_score() {
        Match matchReplicated = createMatchInit("SPAIN", "PORTUGAL");
        Match match = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.updateScore("SPAIN", "PORTUGAL", 2, 0);
        });

        assertEquals("Only can sum one score and one goal", thrown.getMessage());
    }

    @Test
    @DisplayName("should not update score sum two score")
    public void should_not_update_score_sum_two_score() {
        Match matchReplicated = createMatchInit("SPAIN", "PORTUGAL");
        Match match = createMatchInit("PORTUGAL", "SPAIN");
        when(repository.findByMatchActive("SPAIN", "PORTUGAL", Status.IN_PROGRESS)).thenReturn(Optional.of(matchReplicated));
        when(repository.save(any(Match.class))).thenReturn(match);

        //asserts
        MatchException thrown = assertThrows(MatchException.class, () -> {
            matchService.updateScore("SPAIN", "PORTUGAL", 7, 2);
        });

        assertEquals("Only can sum one score", thrown.getMessage());
    }

    @Test
    @DisplayName("should show all scores sort correctly page one")
    public void should_show_all_scores_sort_correctly_page_one() {
        Page<Match> page = new PageImpl<>(Arrays.asList(matchOne, matchTwo, matchThree));
        Pageable pageable = PageRequest.of(1, 3);
        when(repository.findSummaryGames(pageable, null)).thenReturn(page);


        Page<Match> pageResult = matchService.summaryMatchTotalScore(pageable, null);

        //assert
        assertEquals(pageResult.getTotalElements(), page.getTotalElements());
    }

    @Test
    @DisplayName("should show all scores sort correctly in progress")
    public void should_show_all_scores_sort_correctly_in_progress() {
        Page<Match> page = new PageImpl<>(Arrays.asList(matchOne, matchTwo));
        Pageable pageable = PageRequest.of(1, 3);
        when(repository.findSummaryGames(pageable, Status.IN_PROGRESS)).thenReturn(page);

        Page<Match> pageResult = matchService.summaryMatchTotalScore(pageable, Status.IN_PROGRESS);

        //assert
        assertEquals(pageResult.getTotalElements(), 2);
    }

    @Test
    @DisplayName("should show all scores sort correctly finished")
    public void should_show_all_scores_sort_correctly_finished() {
        Page<Match> page = new PageImpl<>(Arrays.asList(matchOne));
        Pageable pageable = PageRequest.of(1, 3);
        when(repository.findSummaryGames(pageable, Status.FINISHED)).thenReturn(page);

        Page<Match> pageResult = matchService.summaryMatchTotalScore(pageable, Status.FINISHED);

        //assert
        assertEquals(pageResult.getTotalElements(), 1);
    }

    @Test
    @DisplayName("should show all scores sort correctly empty")
    public void should_show_all_scores_sort_correctly_empty() {
        Page<Match> page = Page.empty();
        Pageable pageable = PageRequest.of(1, 3);
        when(repository.findSummaryGames(pageable, null)).thenReturn(page);


        Page<Match> pageResult = matchService.summaryMatchTotalScore(pageable, null);

        //assert
        assertEquals(pageResult.getTotalElements(), page.getTotalElements());
    }

    @Test
    @DisplayName("should not show nothing with page not valid")
    public void should_not_show_nothing_with_page_not_valid() {
        //asserts
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
            Pageable pageable = PageRequest.of(-1, 3);
            matchService.summaryMatchTotalScore(pageable, null);
        });

        assertEquals("Page index must not be less than zero", thrown.getMessage());
    }

    private final Match createMatchInit(String homeTeam, String awayTeam) {
        return Match
                .builder()
                .homeTeam(homeTeam)
                .awayTeam(awayTeam)
                .homeScore(0)
                .awayScore(0)
                .startDate(LocalDateTime.now())
                .endDate(null)
                .status(Status.IN_PROGRESS)
                .build();
    }

    private final Match createMatch(Long id, String homeTeam, String awayTeam, Integer homeScore, Integer awayScore, LocalDateTime startDate, Status status) {
        LocalDateTime endDate = null;
        if (status.equals(Status.FINISHED)) {
            endDate = calculateEndMatch(startDate);
        }
        return Match
                .builder()
                .id(id)
                .homeTeam(homeTeam)
                .awayTeam(awayTeam)
                .homeScore(homeScore)
                .awayScore(awayScore)
                .startDate(startDate)
                .endDate(endDate)
                .status(status)
                .build();
    }

    private final LocalDateTime calculateEndMatch(LocalDateTime startDate) {
        //Calculate ideal time match
        return startDate.plusMinutes(105);
    }
}