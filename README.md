# Football World Cup Score Board

You are working on a sports data company, and we would like you to develop a new Live
Football World Cup Score Board that shows matches and scores.
The board supports the following operations:
1. Start a game. When a game starts, it should capture (being initial score 0 – 0):
   a. Home team
   b. Away team
2. Finish game. It will remove a match from the scoreboard.
3. Update score. Receiving the pair score; home team score and away team score
   updates a game score.
4. Get a summary of games by total score. Those games with the same total score will
   be returned ordered by the most recently added to our system.

## Install

    mvn clean install

## Run the app

    mvn spring-boot:run

## Run the tests

    mvn surefire-report:report

# Exercises

For these exercises has been implemented a spring project use an in-memory store (h2). 

The model used is:

```JSON
{
  "id": "1", 
  "homeTeam": "SPAIN", 
  "awayTeam": "PORTUGAL", 
  "homeScore": 2,
  "awayScore": 0,
  "status": "IN_PROGRESS",
  "startDate": "2022-10-05 10:08:02",
  "endDate":"2022-10-05 12:08:02"
}
```


## Start a game

### Service

`Match startGame(String homeTeam, String awayTeam)`

This service allows to create a match given two strings. It is controlled that the two values entered are not equal and that the match is not in progress

## Finish game

### Service

`Match finishGame(String homeTeam, String awayTeam)`

This service allows to remove a match given two strings. It is controlled that the match is in progress and the match exist in database.

## Update score

### Service

`Match updateScore(String homeTeam, String awayTeam, Integer homeScore, Integer awayScore)`

This service update score of match. It is controlled that the match is in progress and the match exist in database. The entered marker can only update one score and it can only add a goal.

## Get a summary of games by total score

### Service

`Page<Match> summaryMatchTotalScore(Pageable pageable, Status status)`

This service show summary of games by total score. The result is a Pageable object. The sort by default those games with the same total score will
be returned ordered by the most recently added to our system. it is possible filter by status if the user insert null the result are all register in database.


# Other information

* [Spring boot](https://spring.io/projects/spring-boot)
* [Test (Junit 5)](https://junit.org/junit5/)
* [Test (Surefire)](https://maven.apache.org/surefire/maven-surefire-report-plugin/usage.html)
* [Database (h2)](https://www.h2database.com/html/main.html)
* [Repository (JPA)](https://spring.io/projects/spring-data-jpa)
* [Lombok](https://projectlombok.org/)
